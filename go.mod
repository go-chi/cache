module gitea.com/go-chi/cache

go 1.21

require (
	github.com/bradfitz/gomemcache v0.0.0-20190329173943-551aad21a668
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/go-sql-driver/mysql v1.4.1
	github.com/lib/pq v1.2.0
	github.com/lunny/nodb v0.0.0-20160621015157-fc1ef06ad4af
	github.com/siddontang/ledisdb v0.0.0-20190202134119-8ceb77e66a92
	github.com/stretchr/testify v1.7.1
	github.com/unknwon/com v0.0.0-20190804042917-757f69c95f3e
	golang.org/x/crypto v0.0.0-20190605123033-f99c8df09eb5
	gopkg.in/ini.v1 v1.44.2
)

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/cupcake/rdb v0.0.0-20161107195141-43ba34106c76 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/edsrzf/mmap-go v1.0.0 // indirect
	github.com/golang/snappy v0.0.2 // indirect
	github.com/lunny/log v0.0.0-20160921050905-7887c61bf0de // indirect
	github.com/mattn/go-sqlite3 v1.11.0 // indirect
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/pelletier/go-toml v1.8.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/siddontang/go v0.0.0-20180604090527-bdc77568d726 // indirect
	github.com/siddontang/go-snappy v0.0.0-20140704025258-d8f7bb82a96d // indirect
	github.com/siddontang/rdb v0.0.0-20150307021120-fc89ed2e418d // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80 // indirect
	golang.org/x/sys v0.0.0-20190730183949-1393eb018365 // indirect
	google.golang.org/appengine v1.6.1 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
